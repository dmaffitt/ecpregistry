package edu.wustl.mir.ctt.form;

import edu.wustl.mir.ctt.model.AttributeDate;
import edu.wustl.mir.ctt.model.AttributeInteger;
import edu.wustl.mir.ctt.model.AttributeString;
import edu.wustl.mir.ctt.model.ECPFormTypes;
import edu.wustl.mir.ctt.model.VerificationStatus;
import java.util.Date;

/**
 *
 * @author pkc
 */
public class ChangeTherapyForm extends BasicForm {
    
    public static final String[] SourceDocumentTypes = new String[]{"Clinical Note or Medication Record Form"};

    public ChangeTherapyForm() {
        // constructor
        super();
        formType = ECPFormTypes.CHANGE_THERAPY;
        title = "Change in Therapy Form";
        this.sourceDocumentTypes = SourceDocumentTypes;
        this.requiresValidation = false;
        this.requiresVerification = false;
        
        if ("1.0".equals(this.getCrfVersion()) || "5.0".equals(this.getCrfVersion())) {
            this.requiresSourceDoc = false;
        } else {
            this.sourceDocumentTypes = SourceDocumentTypes;
        }

        attributes.put("changeTherapyDate", new AttributeDate("changeTherapyDate")); 
        attributes.put("changeTherapy", new AttributeString("changeTherapy"));
        attributes.put("tacrolimusCurrent", new AttributeString("tacrolimusCurrent"));
        attributes.put("tacrolimusCurrentDosage", new AttributeString("tacrolimusCurrentDosage"));
        attributes.put("alemtuzumabCurrent", new AttributeString("alemtuzumabCurrent"));
        attributes.put("alemtuzumabCurrentDosage", new AttributeString("alemtuzumabCurrentDosage"));
        attributes.put("sirolimusCurrent", new AttributeString("sirolimusCurrent"));
        attributes.put("sirolimusCurrentDosage", new AttributeString("sirolimusCurrentDosage"));
        attributes.put("everolimusCurrent", new AttributeString("everolimusCurrent"));
        attributes.put("everolimusCurrentDosage", new AttributeString("everolimusCurrentDosage"));
        attributes.put("azathioprineCurrent", new AttributeString("azathioprineCurrent"));
        attributes.put("azathioprineCurrentDosage", new AttributeString("azathioprineCurrentDosage"));
        attributes.put("cyclosporineACurrent", new AttributeString("cyclosporineACurrent"));
        attributes.put("cyclosporineACurrentDosage", new AttributeString("cyclosporineACurrentDosage"));
        attributes.put("methotrexateCurrent", new AttributeString("methotrexateCurrent"));
        attributes.put("methotrexateCurrentDosage", new AttributeString("methotrexateCurrentDosage"));
        attributes.put("macrolideAntibioticCurrent", new AttributeString("macrolideAntibioticCurrent"));
        attributes.put("macrolideAntibioticCurrentDosage", new AttributeString("macrolideAntibioticCurrentDosage"));
        attributes.put("mycophenolateMofetilCurrent", new AttributeString("mycophenolateMofetilCurrent"));
        attributes.put("mycophenolateMofetilCurrentDosage", new AttributeString("mycophenolateMofetilCurrentDosage"));
        attributes.put("antiThymocyteGlobulinCurrent", new AttributeString("antiThymocyteGlobulinCurrent"));
        attributes.put("antiThymocyteGlobulinCurrentDosage", new AttributeString("antiThymocyteGlobulinCurrentDosage"));
        attributes.put("totalLymphoidIrradiationCurrent", new AttributeString("totalLymphoidIrradiationCurrent"));
        attributes.put("otherDrugsCurrent", new AttributeString("otherDrugsCurrent"));        
        attributes.put("otherDrugNamesCurrent", new AttributeString("otherDrugNamesCurrent"));        
        
        attributes.put("prednisoneCurrent", new AttributeString("prednisoneCurrent"));
        attributes.put("prednisoneCurrentDosage", new AttributeInteger("prednisoneCurrentDosage"));
        attributes.put("prednisoneDosageChanges", new AttributeInteger("prednisoneDosageChanges"));
        attributes.put("prednisoneLowestDose", new AttributeInteger("prednisoneLowestDose"));
        attributes.put("prednisoneHighestDose", new AttributeInteger("prednisoneHighestDose"));
        
        attributes.put("onAntiCoagulant", new AttributeString("onAntiCoagulant"));
        attributes.put("antiCoagulantName1", new AttributeString("antiCoagulantName1"));
        attributes.put("antiCoagulantName2", new AttributeString("antiCoagulantName2"));
        attributes.put("antiCoagulantName3", new AttributeString("antiCoagulantName3"));
        attributes.put("onAntiPlatelet", new AttributeString("onAntiPlatelet"));
        attributes.put("antiPlateletName1", new AttributeString("antiPlateletName1"));
        attributes.put("antiPlateletName2", new AttributeString("antiPlateletName2"));
        attributes.put("antiPlateletName3", new AttributeString("antiPlateletName3"));

        attributes.put("ecpTherapyDiscontinued", new AttributeString("ecpTherapyDiscontinued"));
        attributes.put("ecpTherapyDiscontinuedDate", new AttributeDate("ecpTherapyDiscontinuedDate"));        
        attributes.put("ecpTherapyDiscontinuedReason", new AttributeString("ecpTherapyDiscontinuedReason"));

        attributes.put("comment", new AttributeString("comment"));

        this.clear();
    }
    
    public ChangeTherapyForm( BasicForm bf) {
        super(bf);		
        title = bf.getTitle();
        this.sourceDocumentTypes = SourceDocumentTypes;
        this.requiresVerification = false;
        
        if ("1.0".equals(this.getCrfVersion()) || "5.0".equals(this.getCrfVersion())) {
            this.requiresSourceDoc = false;
        } else {
            this.sourceDocumentTypes = SourceDocumentTypes;
        }
    }
	
    public Date getChangeTherapyDate() {
        return (Date) attributes.get("changeTherapyDate").getValue();
    }
    
    public void setChangeTherapyDate(Date changeTherapyDate) {
        attributes.get("changeTherapyDate").setValue(changeTherapyDate);
    }

    
    public String getChangeTherapy() {
//        System.out.println("getBaselineTherapyVerificationStatus was called containing: " + attributes.get("baselineTherapy").getVerificationStatus());
        return (String) attributes.get("changeTherapy").getValue();
    }
    
    public void setChangeTherapy(String changeTherapy) {
//        System.out.println("SET BaselineTherapyVerificationStatus was called containing: " + verificationStatus);
        attributes.get("changeTherapy").setValue(changeTherapy);
    }

    
    public String getTacrolimusCurrent() {
        return (String) attributes.get("tacrolimusCurrent").getValue();
    }
    
    public void setTacrolimusCurrent(String tacrolimusCurrent) {
        attributes.get("tacrolimusCurrent").setValue(tacrolimusCurrent);
    }

    public String getTacrolimusCurrentDosage() {
        return (String) attributes.get("tacrolimusCurrentDosage").getValue();
    }
    
    public void setTacrolimusCurrentDosage(String tacrolimusCurrentDosage) {
        attributes.get("tacrolimusCurrentDosage").setValue(tacrolimusCurrentDosage);
    }


    public String getPrednisoneCurrent() {
        return (String) attributes.get("prednisoneCurrent").getValue();
    }
    
    public void setPrednisoneCurrent(String prednisoneCurrent) {
        attributes.get("prednisoneCurrent").setValue(prednisoneCurrent);
    }


    public Integer getPrednisoneCurrentDosage() {
        return (Integer) attributes.get("prednisoneCurrentDosage").getValue();
    }
    
    public void setPrednisoneCurrentDosage(Integer prednisoneCurrentDosage) {
        attributes.get("prednisoneCurrentDosage").setValue(prednisoneCurrentDosage);
    }

    public Integer getPrednisoneDosageChanges() {
        return attributes.containsKey("prednisoneDosageChanges") ? (Integer)attributes.get("prednisoneDosageChanges").getValue() : null;
    }
    
    public void setPrednisoneDosageChanges(Integer prednisoneDosageChanges) {
        if (!attributes.containsKey("prednisoneDosageChanges")) {
            attributes.put("prednisoneDosageChanges", new AttributeInteger("prednisoneDosageChanges"));
        }
        
        attributes.get("prednisoneDosageChanges").setValue(prednisoneDosageChanges);
    }
    
    public Integer getPrednisoneLowestDose() {
        return attributes.containsKey("prednisoneLowestDose") ? (Integer) attributes.get("prednisoneLowestDose").getValue() : null;
    }
    
    public void setPrednisoneLowestDose(Integer prednisoneLowestDose) {
        if (!attributes.containsKey("prednisoneLowestDose")) {
            attributes.put("prednisoneLowestDose", new AttributeInteger("prednisoneLowestDose"));
        }
        
        attributes.get("prednisoneLowestDose").setValue(prednisoneLowestDose);
    }
    
    public Integer getPrednisoneHighestDose() {
        return attributes.containsKey("prednisoneHighestDose") ? (Integer) attributes.get("prednisoneHighestDose").getValue() : null;
    }
    
    public void setPrednisoneHighestDose(Integer prednisoneHighestDose) {
        if (!attributes.containsKey("prednisoneHighestDose")) {
            attributes.put("prednisoneHighestDose", new AttributeInteger("prednisoneHighestDose"));
        }
        
        attributes.get("prednisoneHighestDose").setValue(prednisoneHighestDose);
    }

    public String getAlemtuzumabCurrent() {
        return (String) attributes.get("alemtuzumabCurrent").getValue();
    }
    
    public void setAlemtuzumabCurrent(String alemtuzumabCurrent) {
        attributes.get("alemtuzumabCurrent").setValue(alemtuzumabCurrent);
    }

    public String getAlemtuzumabCurrentDosage() {
        return (String) attributes.get("alemtuzumabCurrentDosage").getValue();
    }
    
    public void setAlemtuzumabCurrentDosage(String alemtuzumabCurrentDosage) {
        attributes.get("alemtuzumabCurrentDosage").setValue(alemtuzumabCurrentDosage);
    }

    
    public String getSirolimusCurrent() {
        return (String) attributes.get("sirolimusCurrent").getValue();
    }
    
    public void setSirolimusCurrent(String sirolimusCurrent) {
        attributes.get("sirolimusCurrent").setValue(sirolimusCurrent);
    }

    public String getSirolimusCurrentDosage() {
        return (String) attributes.get("sirolimusCurrentDosage").getValue();
    }
    
    public void setSirolimusCurrentDosage(String sirolimusCurrentDosage) {
        attributes.get("sirolimusCurrentDosage").setValue(sirolimusCurrentDosage);
    }


    public String getEverolimusCurrent() {
        return (String) attributes.get("everolimusCurrent").getValue();
    }
    
    public void setEverolimusCurrent(String everolimusCurrent) {
        attributes.get("everolimusCurrent").setValue(everolimusCurrent);
    }

    public String getEverolimusCurrentDosage() {
        return (String) attributes.get("everolimusCurrentDosage").getValue();
    }
    
    public void setEverolimusCurrentDosage(String everolimusCurrentDosage) {
        attributes.get("everolimusCurrentDosage").setValue(everolimusCurrentDosage);
    }


    public String getAzathioprineCurrent() {
        return (String) attributes.get("azathioprineCurrent").getValue();
    }
    
    public void setAzathioprineCurrent(String azathioprineCurrent) {
        attributes.get("azathioprineCurrent").setValue(azathioprineCurrent);
    }

    public String getAzathioprineCurrentDosage() {
        return (String) attributes.get("azathioprineCurrentDosage").getValue();
    }
    
    public void setAzathioprineCurrentDosage(String azathioprineCurrentDosage) {
        attributes.get("azathioprineCurrentDosage").setValue(azathioprineCurrentDosage);
    }


    public String getCyclosporineACurrent() {
        return (String) attributes.get("cyclosporineACurrent").getValue();
    }
    
    public void setCyclosporineACurrent(String cyclosporineACurrent) {
        attributes.get("cyclosporineACurrent").setValue(cyclosporineACurrent);
    }

    public String getCyclosporineACurrentDosage() {
        return (String) attributes.get("cyclosporineACurrentDosage").getValue();
    }
    
    public void setCyclosporineACurrentDosage(String cyclosporineACurrentDosage) {
        attributes.get("cyclosporineACurrentDosage").setValue(cyclosporineACurrentDosage);
    }


    public String getMethotrexateCurrent() {
        return (String) attributes.get("methotrexateCurrent").getValue();
    }
    
    public void setMethotrexateCurrent(String methotrexateCurrent) {
        attributes.get("methotrexateCurrent").setValue(methotrexateCurrent);
    }

    public String getMethotrexateCurrentDosage() {
        return (String) attributes.get("methotrexateCurrentDosage").getValue();
    }
    
    public void setMethotrexateCurrentDosage(String methotrexateCurrentDosage) {
        attributes.get("methotrexateCurrentDosage").setValue(methotrexateCurrentDosage);
    }


    public String getMacrolideAntibioticCurrent() {
        return (String) attributes.get("macrolideAntibioticCurrent").getValue();
    }
    
    public void setMacrolideAntibioticCurrent(String macrolideAntibioticCurrent) {
        attributes.get("macrolideAntibioticCurrent").setValue(macrolideAntibioticCurrent);
    }

    public String getMacrolideAntibioticCurrentDosage() {
        return (String) attributes.get("macrolideAntibioticCurrentDosage").getValue();
    }
    
    public void setMacrolideAntibioticCurrentDosage(String macrolideAntibioticCurrentDosage) {
        attributes.get("macrolideAntibioticCurrentDosage").setValue(macrolideAntibioticCurrentDosage);
    }


    public String getMycophenolateMofetilCurrent() {
        return (String) attributes.get("mycophenolateMofetilCurrent").getValue();
    }
    
    public void setMycophenolateMofetilCurrent(String mycophenolateMofetilCurrent) {
        attributes.get("mycophenolateMofetilCurrent").setValue(mycophenolateMofetilCurrent);
    }

    public String getMycophenolateMofetilCurrentDosage() {
        return (String) attributes.get("mycophenolateMofetilCurrentDosage").getValue();
    }
    
    public void setMycophenolateMofetilCurrentDosage(String mycophenolateMofetilCurrentDosage) {
        attributes.get("mycophenolateMofetilCurrentDosage").setValue(mycophenolateMofetilCurrentDosage);
    }


    public String getAntiThymocyteGlobulinCurrent() {
        return (String) attributes.get("antiThymocyteGlobulinCurrent").getValue();
    }
    
    public void setAntiThymocyteGlobulinCurrent(String antiThymocyteGlobulinCurrent) {
        attributes.get("antiThymocyteGlobulinCurrent").setValue(antiThymocyteGlobulinCurrent);
    }

    public String getAntiThymocyteGlobulinCurrentDosage() {
        return (String) attributes.get("antiThymocyteGlobulinCurrentDosage").getValue();
    }
    
    public void setAntiThymocyteGlobulinCurrentDosage(String antiThymocyteGlobulinCurrentDosage) {
        attributes.get("antiThymocyteGlobulinCurrentDosage").setValue(antiThymocyteGlobulinCurrentDosage);
    }

    
    public String getTotalLymphoidIrradiationCurrent() {
        return (String) attributes.get("totalLymphoidIrradiationCurrent").getValue();
    }
    
    public void setTotalLymphoidIrradiationCurrent(String totalLymphoidIrradiationCurrent) {
        attributes.get("totalLymphoidIrradiationCurrent").setValue(totalLymphoidIrradiationCurrent);
    }
    
    public String getOtherDrugsCurrent() {
        return (String) attributes.get("otherDrugsCurrent").getValue();
    }
    
    public void setOtherDrugsCurrent(String OtherDrugsCurrent) {
        attributes.get("otherDrugsCurrent").setValue(OtherDrugsCurrent);
    }
        
    
    public String getOtherDrugNamesCurrent() {
        return (String) attributes.get("otherDrugNamesCurrent").getValue();
    }
    
    public void setOtherDrugNamesCurrent(String OtherDrugNamesCurrent) {
        attributes.get("otherDrugNamesCurrent").setValue(OtherDrugNamesCurrent);
    }
        
    
    public String getOnAntiCoagulant() {
        return (String) attributes.get("onAntiCoagulant").getValue();
    }
    
    public void setOnAntiCoagulant(String onAntiCoagulant) {
        attributes.get("onAntiCoagulant").setValue(onAntiCoagulant);
    }

    public String getAntiCoagulantName1() {
        return (String) attributes.get("antiCoagulantName1").getValue();
    }
    
    public void setAntiCoagulantName1(String antiCoagulantName1) {
        attributes.get("antiCoagulantName1").setValue(antiCoagulantName1);
    }

    public String getAntiCoagulantName2() {
        return (String) attributes.get("antiCoagulantName2").getValue();
    }
    
    public void setAntiCoagulantName2(String antiCoagulantName2) {
        attributes.get("antiCoagulantName2").setValue(antiCoagulantName2);
    }

    public String getAntiCoagulantName3() {
        return (String) attributes.get("antiCoagulantName3").getValue();
    }
    
    public void setAntiCoagulantName3(String antiCoagulantName3) {
        attributes.get("antiCoagulantName3").setValue(antiCoagulantName3);
    }

    
    
    public String getOnAntiPlatelet() {
        return (String) attributes.get("onAntiPlatelet").getValue();
    }
    
    public void setOnAntiPlatelet(String onAntiPlatelet) {
        attributes.get("onAntiPlatelet").setValue(onAntiPlatelet);
    }

    public String getAntiPlateletName1() {
        return (String) attributes.get("antiPlateletName1").getValue();
    }
    
    public void setAntiPlateletName1(String antiPlateletName1) {
        attributes.get("antiPlateletName1").setValue(antiPlateletName1);
    }

    public String getAntiPlateletName2() {
        return (String) attributes.get("antiPlateletName2").getValue();
    }
    
    public void setAntiPlateletName2(String antiPlateletName2) {
        attributes.get("antiPlateletName2").setValue(antiPlateletName2);
    }

    public String getAntiPlateletName3() {
        return (String) attributes.get("antiPlateletName3").getValue();
    }
    
    public void setAntiPlateletName3(String antiPlateletName3) {
        attributes.get("antiPlateletName3").setValue(antiPlateletName3);
    }
    
    
    

    public String getEcpTherapyDiscontinued() {
        return (String) attributes.get("ecpTherapyDiscontinued").getValue();
    }
    
    public void setEcpTherapyDiscontinued(String ecpTherapyDiscontinued) {
        attributes.get("ecpTherapyDiscontinued").setValue(ecpTherapyDiscontinued);
    }



    public Date getEcpTherapyDiscontinuedDate() {
        return (Date) attributes.get("ecpTherapyDiscontinuedDate").getValue();
    }
    
    public void setEcpTherapyDiscontinuedDate(Date ecpTherapyDiscontinuedDate) {
        attributes.get("ecpTherapyDiscontinuedDate").setValue(ecpTherapyDiscontinuedDate);
    }

    public String getEcpTherapyDiscontinuedReason() {
        return (String) attributes.get("ecpTherapyDiscontinuedReason").getValue();
    }
    
    public void setEcpTherapyDiscontinuedReason(String ecpTherapyDiscontinuedReason) {
        attributes.get("ecpTherapyDiscontinuedReason").setValue(ecpTherapyDiscontinuedReason);
    }

    public String getComment() {
        return (String) attributes.get("comment").getValue();
    }
    
    public void setComment(String comment) {
        attributes.get("comment").setValue(comment);
    }


}
