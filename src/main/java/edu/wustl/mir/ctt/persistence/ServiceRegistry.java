package edu.wustl.mir.ctt.persistence;

import edu.wustl.mir.ctt.auditlog.AuditLogException;
import edu.wustl.mir.ctt.auditlog.AuditLogManager;
import edu.wustl.mir.ctt.auditlog.JDBCAuditLogManager;
import edu.wustl.mir.ctt.directory.DirectoryManager;
import edu.wustl.mir.ctt.directory.DirectoryManagerException;
import edu.wustl.mir.ctt.directory.LDAPDirectoryManager;
import edu.wustl.mir.ctt.notification.NotificationException;
import edu.wustl.mir.ctt.notification.NotificationManager;
import edu.wustl.mir.ctt.notification.properties.EmailNotificationManager;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;

public class ServiceRegistry {

    private static PersistenceManager pm;
    private static ReportPersistenceManager rpm;
    private static AuditLogManager alm;
    private static NotificationManager nm;
    private static DirectoryManager dm;
    
    private static final Logger logger = Logger.getLogger(ServiceRegistry.class);

    public static PersistenceManager getPersistenceManager() throws PersistenceException {
        if(pm == null) {
            pm = new JDBCPersistenceManager();
        }
        return pm;
    }

    public static ReportPersistenceManager getReportPersistenceManager() throws PersistenceException {
        if(rpm == null) {
            rpm = new JDBCReportPersistenceManager();
        }
        return rpm;
    }

    public static AuditLogManager getAuditLogManager() throws AuditLogException {
        if(alm == null) {
            alm = new JDBCAuditLogManager();
        }
        return alm;
    }

    public static NotificationManager getNotificationManager() throws NotificationException {
        if(nm == null) {
            try {
                Context env = (Context) new InitialContext().lookup("java:comp/env");
                String smtphost = (String)env.lookup("smtpHost");
                
                String enrollmentListFileName = (String)env.lookup("enrollmentNotificationEmailList");
                String saeListFileName = (String)env.lookup("saeNotificationEmailList");
                
                nm = new EmailNotificationManager( enrollmentListFileName, saeListFileName, smtphost);
            }
            catch( NamingException | NotificationException e) {
                String msg = "Failed to initialize Notification Manager.";
                logger.error(msg, e);
                throw new NotificationException( msg, e);
            }
        }
        return nm;
    }

    public static DirectoryManager getDirectoryManager() throws DirectoryManagerException {
        if( dm == null) {
//            dm = new LDAPDirectoryManager("ldap://localhost:1389", "cn=Directory Manager", "ldapDMbrussels");
            
            // anonymous read-only access.
// NOTE: The following three lines that are commented out were used before Maven and before Paul installed ApacheDS for LDAP for port 1389.
// The following line of code with 128.252.175.92 is needed so the software will run correctly on Paul K. Commean's rehab3 windows 7 computer.
////            dm = new LDAPDirectoryManager("ldap://128.252.175.92:1389", "", "");   // ECP Test server IP address to gain access to LDAP
////            dm = new LDAPDirectoryManager("ldap://10.252.175.80:1389", "", "");   // Dave Maffitt's computer in his office to gain access to LDAP
            dm = new LDAPDirectoryManager("ldaps://nrg.mir", "ecp_ldap@nrg.mir", "Ehae1ahnah6iezahMah6rae9C");
        }
        return dm;
    }
}
