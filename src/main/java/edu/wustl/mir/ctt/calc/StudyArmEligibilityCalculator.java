package edu.wustl.mir.ctt.calc;

import edu.wustl.mir.ctt.form.StudyArmEligibilityForm;
import edu.wustl.mir.ctt.model.PulmonaryEvaluation;
import edu.wustl.mir.ctt.util.DateUtil;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.time.DateUtils;

/**
 * Screen the participant for eligibility and study-arm assignment.
 * 
 * Uses all measurements provided. Only screens for minimum data requirements.
 *
 * @author drm
 */
public class StudyArmEligibilityCalculator implements Serializable {
    
    private String[] rules = {
        "in the previous 6 months, there are not at least 5 measurements that are all spaced at least 7 days apart.",   // 0: not eligible
        "the most recent measurement is older than 7 days.",                                                            // 1: not eligible
        "meets the criteria when the minimum FEV1 >= 1200 ml (slope < -30 AND p-value < 0.05).",                        // 2: ecp-treatment eligible
        "because the p-value >= 0.05 when the minimum FEV1 >= 1200 ml and the slope < -30",                             // 3: observational-arm eligible
        "because the slope >= -30 when the minimum FEV1 >= 1200 ml",                                                    // 4: observational-arm eligible
        "meets the criteria when the minimum FEV1 < 1200 ml (slope < -10 AND p-value < 0.05).",                         // 5: ecp-treatment eligible
        "because the p-value >= 0.05 when the minimum FEV1 < 1200 ml and the slope < -10",                              // 6: observational-arm eligible
        "because the slope >= -10 when the minimum FEV1 < 1200 ml"                                                      // 7: observational-arm eligible
    };
   
    private String[] msgs = {
        "Patient not eligible",
        "Patient not eligible",
        "ECP-Treatment Arm eligible. Complete enrollment eligibility forms on line to confirm eligibility",
        "Observational Arm eligible. Complete enrollment eligibility forms on line to confirm eligibility",
        "Observational Arm eligible. Complete enrollment eligibility forms on line to confirm eligibility",
        "ECP-Treatment Arm eligible. Complete enrollment eligibility forms on line to confirm eligibility",
        "Observational Arm eligible. Complete enrollment eligibility forms on line to confirm eligibility",
        "Observational Arm eligible. Complete enrollment eligibility forms on line to confirm eligibility"
    };
    
    private SlopeEstimator estimator;
    private int index;
    private float minFev;
    private float lastFev;
    private float slope;
    private float pvalue;
    private List<PulmonaryEvaluation> evals;
    private List<PulmonaryEvaluation> evalsWithQualifyingDates;
    private Date screenDate;
    private String fev1OldestPossibleDate;
    
    public StudyArmEligibilityCalculator( StudyArmEligibilityForm form) {
        this( form.getPulmEvaluations(), form.getDate());
    }
    
    public StudyArmEligibilityCalculator( List<PulmonaryEvaluation> evals, Date screenDate) {
        this.evals = evals;
        this.screenDate = screenDate;
//        System.out.println("The screenDate is the form.getDate: " + screenDate);
        index = screen();
    }

    public List<PulmonaryEvaluation> getEvaluations() {
        return evals;
    }

    public List<PulmonaryEvaluation> getEvalsWithQualifyingDates() {
        return evalsWithQualifyingDates;
    }

    public Date getScreenDate() {
        return screenDate;
    }

    public void setScreenDate(Date screenDate) {
        this.screenDate = screenDate;
    }
    
    public String getOutcomeMessage() {
        return msgs[index];
    }
    
    public String getOutcomeRule() {
        return rules[index];
    }
    
    public String[] getRules() {
        return rules;
    }
    
    public float getPValue() {
        return pvalue;
    }
    
    public float getSlope() {
        return slope;
    }
    
    public double predict( Date d) {
        // If you get an error message whne trying to run the estimator.predict(d) command, it most likely is due to the esitmator not being
        // instantiated in the screen() method below (see the line of code 'estimator = new SlopeEstimator( dates, fevs)' in the screen() method).
        return estimator.predict(d);
    }
    
    public float getMinFev() {
        return minFev;
    }
    
    public float getLastFev() {
        return lastFev;
    }
    
    public String getFev1OldestPossibleDate() {
        return fev1OldestPossibleDate;
    }
    
    public boolean isECPTreatmentArmEligible() {
        boolean b = false;
        if( index == 2 || index == 5) b = true;
        return b;
    }
    
    public boolean isObservationalArmEligible() {
        boolean b = false;
        if( index == 3 || index == 4 || index == 6 || index == 7) b = true;
        return b;
    }
    
    public boolean isSlopeOKandStatsSig() {
        return (index == 2) || ( index == 5);
    }
    
    public boolean isSlopeOKandStatsNotSig() {
        return (index == 3) || ( index == 6);
    }
    
    public boolean isDataStale() {
        return index == 1;
    }
    
    public boolean isDataTooFew() {
        return index == 0;
    }
    
    public int numberOfMinimallySpacedMeasurementsInPeriod( List<PulmonaryEvaluation> evals, Date startDate, Date stopDate, float spacingInDays) {
        evalsWithQualifyingDates = filterForMinimalSpacing( evals, spacingInDays);
        
//        int j;
//        for(int i=0; i < qualifyingDates.size(); i++){
//            j = i + 1;
//            System.out.println("The qualifying dates are: " + j + "   " + qualifyingDates.get(i).getDate().toString());
//        }
        return countInInterval( evalsWithQualifyingDates, startDate, stopDate);
    }
    
    public List<PulmonaryEvaluation> filterForMinimalSpacing( List<PulmonaryEvaluation> evals, float spacingInDays) {
        evalsWithQualifyingDates = new ArrayList<PulmonaryEvaluation>();
        
        if( evals.size() > 1) {
            evalsWithQualifyingDates.add( evals.get(0));
            Date d1, d2;
            for( int i = 0; i < evals.size(); i++) {
                d1 = evals.get(i).getDate();
                for( int j = i+1; j < evals.size(); j++) {
                    d2 = evals.get(j).getDate();
                    // The minus 0.1 needed to be taken away from the spacingInDays because of the change
                    // in Central Standard Time (CST) to Daylight Savings Time (DST).  The day of the time  
                    // change, the number of hours in a day are 23 not 24 so a day is less than 1, but we 
                    // expect the days in the interval to all have 24 hours.  The minus 0.1 takes care of 
                    // the change in time in the spring.
                    if( DateUtil.intervalInDays( d1, d2) >= spacingInDays - 0.1) {
                        evalsWithQualifyingDates.add( evals.get(j));
                        i = j - 1;
                        break;
                    }
                }
            }
        }
        
        return evalsWithQualifyingDates;
    }
    
    public int countInInterval( List<PulmonaryEvaluation> evals, Date startDate, Date stopDate) {
        int cnt = 0;
        double days;
        days = DateUtil.intervalInDays(startDate, stopDate);
        for( PulmonaryEvaluation e: evals) {
            if( DateUtil.isDateInIntervalInclusive(e.getDate(), startDate, stopDate)) {
                cnt++;
            }
        }
        System.out.println("The count in the 6 month interval is: " + cnt);
        return cnt;
    }
    
    private boolean getAreMeasurementsInTimeBounds() {
        for( PulmonaryEvaluation e: evals) {
            Date startDate = DateUtils.addDays(screenDate, -182);
            if( ! DateUtil.isDateInIntervalInclusive(e.getDate(), startDate, screenDate)) {
                return false;
            }
        }
        return true;
    }
    
    private boolean isMeasurementWithinLastSevenDays( Date d, Date now) {
        double days = DateUtil.intervalInDays(d, now);
        return days < 8;
//        Date startDate = DateUtil.addDays(now, -7);
//        return DateUtil.isDateInIntervalInclusive( d, startDate, now);
    }
    
    private Date getDateOfLastEvaluation() {
        Date d = null;
        if( evals.size() > 0) {
            d = evals.get(0).getDate();
            for( PulmonaryEvaluation e: evals) {
                if( e.getDate().after(d)) {
                    d = e.getDate();
                }
            }
        }
        return d;
    }
    
    public List<PulmonaryEvaluation> getEvalsWithinLastSixMonths(List<PulmonaryEvaluation> pulmEvals, Date screenDate) throws ParseException{
//        List<PulmonaryEvaluation> pulmEvals = new ArrayList<PulmonaryEvaluation>();
        List<PulmonaryEvaluation> pulmEvals2 = new ArrayList<PulmonaryEvaluation>();
        
        // Sort the array of PulmonaryEvaluation objects by date in ascending order.
        Collections.sort(pulmEvals, new Comparator<PulmonaryEvaluation>() {
            @Override
            public int compare(PulmonaryEvaluation pe1, PulmonaryEvaluation pe2) {
                if (pe1.getDate() == null || pe2.getDate() == null)
                    return 0;
                return pe1.getDate().compareTo(pe2.getDate());
            }
        });

        // Remove any dates older than six months from the list.
        PulmonaryEvaluation pe;
        // Changed days from double to int so if the DateUtil returns 182.?? days
        // the int will drop the decimal. 
        int days = 0;
        Iterator iterator = pulmEvals.iterator();
        while(iterator.hasNext()){
            pe = (PulmonaryEvaluation) iterator.next();
            days = (int) DateUtil.intervalInDays(pe.getDate(), screenDate);
            if(days < 183){
                pulmEvals2.add(pe);
            }
        }

        return pulmEvals2;

    }
    
    
    private int screen() {
        List<Date> dates = new ArrayList<Date>();
        List<Float> fevs = new ArrayList<Float>();
        int i;
        
        // Format the start of the six month period so the oldest possible FEV1 date can be determined.
        // The fev1OldestPossibleDate is used enrollStudyArmElig.xhtml form in the Omnifaces o:validateMultiple
        // validators to let the individual filling out the form know the oldest possible FEV1 date that can be
        // entered into the form.
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String fev1OldestDate = sdf.format(DateUtil.addDays(screenDate, -183));
        // If a oldest date is 182 days ago, use the date below to show the person entering the dates on the Elig Form.
        // We do not want to show them a date of 183 days old as above.
        fev1OldestPossibleDate = sdf.format(DateUtil.addDays(screenDate, -182));

        // I had to change the fev1OldestDate and startDate to be 183 days because the countInInterval method along
        // with most all of the other methods use hours with the dates.  The hours different in a day or the CST vs CDT
        // can also mess up the calulation of 182 days.  If 183 days is used then it should let through 182 days.
        Date startDate = DateUtil.addDays(screenDate, -183);
        System.out.println("The startDate is the screenDate - 183 days: " + startDate);
        System.out.println("The stopDate is the screenDate: " + screenDate);
        
        try {
            // Get the FEV1 Dates and values within the last six months from the screen date.
            evalsWithQualifyingDates = this.getEvalsWithinLastSixMonths(evals, screenDate);
        } catch (ParseException ex) {
            Logger.getLogger(StudyArmEligibilityCalculator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // SPECIAL NOTE: The number seven (7) used in the call to numberOfMinimallySpacedMeasurementsInPeriod refers to the
        // number of days in the spacing between measurement periods.
        // NOTE: The change from Central Standard Time to Central Daylight Time in the spring causes one day to be only 23 hours long, so
        // a full 7 days does not occur between two dates if the spring time date change is between the dates.
        // If you follow the method numberOfMinimallySpacedMeasurementsInPeriod you will find we subtracted 0.1 days from 7 to account
        // for the change in time.
        int countMeasurementsSufficientlySpaced = numberOfMinimallySpacedMeasurementsInPeriod( evalsWithQualifyingDates, startDate, screenDate, 7);
        System.out.println("The countMeasurementsSufficientlySpaced is: " + countMeasurementsSufficientlySpaced + "\n");
        if( countMeasurementsSufficientlySpaced < 5) {
            index = 0;
            return index;
        }
    
        // Was the last FEV1 (Pulmonary Evaluation) performed within the last 7 days?
        if(  ! isMeasurementWithinLastSevenDays( getDateOfLastEvaluation(), screenDate) ) {
            index = 1;
            return index;
        }

        for( PulmonaryEvaluation eval: evalsWithQualifyingDates) {
            dates.add(eval.getDate());
            fevs.add(eval.getFev1() * 1000f);
        }
                
        int nValues = fevs.size();
        lastFev = fevs.get(nValues - 1);
        int indexOfMinValue = fevs.indexOf( Collections.min(fevs));
        minFev = fevs.get(indexOfMinValue);
        
        estimator = new SlopeEstimator( dates, fevs);
        
        slope = (float) estimator.getSlope();
        pvalue = (float) estimator.getPValue();
        
        if (minFev >= 1200) {
            if( slope < -30) {
                if( pvalue < 0.05) {
                    i = 2;    // minFev >= 1200 and slope < -30 and pvalue < 0.05
                }
                else { // pvalue >= 0.05
                    i = 3;    // minFev >= 1200 and slope < -30 and pvalue >= 0.05
                }
            }
            else { // slope >= -30
                i = 4;    // minFev >= 1200 and slope >= -30
            }

        } else { // minFev < 1200

            if( slope < -10) {
                if( pvalue < 0.05) {
                    i = 5;    // minFev < 1200 and slope < -10 and pvalue < 0.05
                }
                else { // pvalue >= 0.05
                    i = 6;    // minFev < 1200 and slope < -10 and pvalue >= 0.05
                }
            }
            else { // slope >= -10
                i = 7;    // minFev < 1200 and slope >= -10 
            }

        }
            
        return i;
    }
    
//    public static void main( String[] args) {
//        List<PulmonaryEvaluation> evals = new ArrayList<PulmonaryEvaluation>();
//        
//        Calendar cal = Calendar.getInstance();
//        PulmonaryEvaluation eval = new PulmonaryEvaluation(cal.getTime(), 0.0f, 0.0f);
//        evals.add(eval);
//        
//        cal.add(Calendar.DAY_OF_YEAR, 7);
//        eval = new PulmonaryEvaluation(cal.getTime(), 0.0f, 0.0f);
//        evals.add(eval);
//        
//        cal.add(Calendar.DAY_OF_YEAR, 8);
//        eval = new PulmonaryEvaluation(cal.getTime(), 0.0f, 0.0f);
//        evals.add(eval);
//        
//        cal.add(Calendar.DAY_OF_YEAR, 10);
//        eval = new PulmonaryEvaluation(cal.getTime(), 0.0f, 0.0f);
//        evals.add(eval);
//        
//        cal.add(Calendar.DAY_OF_YEAR, 5);
//        eval = new PulmonaryEvaluation(cal.getTime(), 0.0f, 0.0f);
//        evals.add(eval);
//        
//        cal.add(Calendar.DAY_OF_YEAR, 5);
//        eval = new PulmonaryEvaluation(cal.getTime(), 0.0f, 0.0f);
//        evals.add(eval);
//        
//        cal.add(Calendar.DAY_OF_YEAR, 5);
//        eval = new PulmonaryEvaluation(cal.getTime(), 0.0f, 0.0f);
//        evals.add(eval);
//        
//        cal.add(Calendar.DAY_OF_YEAR, 5);
//        eval = new PulmonaryEvaluation(cal.getTime(), 0.0f, 0.0f);
//        evals.add(eval);
//        
//        cal.add(Calendar.DAY_OF_YEAR, 8);
//        eval = new PulmonaryEvaluation(cal.getTime(), 0.0f, 0.0f);
//        evals.add(eval);
//        
//        StudyArmEligibilityCalculator calc = new StudyArmEligibilityCalculator( evals);
//        
//        for( PulmonaryEvaluation e: evals) {
//            System.out.println(e.getDate());
//        }
//        System.out.println( calc.measurementIntervalCount(evals, 6.5f));
//    }
}
