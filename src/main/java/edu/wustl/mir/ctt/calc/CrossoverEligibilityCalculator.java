package edu.wustl.mir.ctt.calc;

import edu.wustl.mir.ctt.form.CrossoverEligibilityWorkSheet;
import edu.wustl.mir.ctt.model.Participant;
import edu.wustl.mir.ctt.model.PulmonaryEvaluation;
import edu.wustl.mir.ctt.persistence.PersistenceException;
import edu.wustl.mir.ctt.persistence.PersistenceManager;
import edu.wustl.mir.ctt.persistence.ServiceRegistry;
import edu.wustl.mir.ctt.util.DateUtil;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author drm
 */
public class CrossoverEligibilityCalculator {
    
    private String[] rules = {
        "Participant is on 2-month hold period.",                                                 // no action 0
        "Fewer than 5 FEV1 values in the last 6 months.",                                         // no action 1
        "The slope meets criteria and is significant.",                                           // To safety check 2
        "The slope may NOT BE OK and/or the significance may NOT BE OK with fewer than 4 FEV1s collected since enrollment date or the end of the 2 month hold period.",  // no action 3
        "4 or more FEV1 values in the last 4 weeks and slope is OK and significant.",             // To safety check 4
        "4 or more FEV1 values in the last 4 weeks and the slope is OK but not significant.",     // To clinical override 5
        "4 or more FEV1 values in the last 4 weeks and the slope is NOT OK.",                     // Keep in observational arm after 4 FEV1s  6
        "No FEV1 values in the last 7 days."                                                      // no action 7
    };
   
    private String[] msgs = {
        "Participant ineligible for crossover assessment - This participant is still within the 2-month hold period and may not be re-assessed for crossover into the ECP Treatment Arm at this time.  This participant remains in the Observational Arm.  The earliest date on which this patient may be re-assessed for crossover is %s.  Please contact your CCC nurse coordinator if you have questions.",
        "Patient ineligible for crossover assessment – Including the most recently entered pulmonary function tests, this patient does not have at least five FEV-1 values that were obtained at least one week apart within the last 6 months, including one FEV-1 value within the last 7 days.  Please obtain and enter subsequent pulmonary assessments before re-assessing for crossover.  Please contact your CCC nurse coordinator if you have questions.",
        "The participant is eligible to cross over into the ECP Treatment Arm.",
        "This patient remains in the Observation Arm because he/she still does not meet the Protocol’s criteria to receive ECP (see Protocol Section 3.6). Please continue to follow the Protocol’s treatment and evaluation procedures for Observation Arm patients.  Please contact your CCC nurse coordinator if you have questions.",
//        "The participant is not eligible to cross over into the Treatment Arm. Collect up to 4 pulmonary evaluations from either the enrollment date or the end of the 2 month hold period date: %s.",
        "The participant is eligible to cross over into the ECP Treatment Arm.",
        "The participant does not meet all of the criteria for inclusion in the ECP treatment arm. Do you wish to override this and move the participant anyway?",
        "The participant does not meet the slope criteria for inclusion in the ECP treatment arm. Keep the participant in the observational arm",
        "The crossover calculator can only be run within 7 days of a pulmonary function evaluation. The last evaluation was done on %s."
    };
    
    private StudyArmEligibilityCalculator eligCalc;
    private int index;
    private Participant p;
    private boolean displayCalculatorResults = false;
    private List<PulmonaryEvaluation> evals;
    private Date screenDate;
    
    public CrossoverEligibilityCalculator( CrossoverEligibilityWorkSheet form) throws PersistenceException {
        this( form.getEvaluations(), form.getDate(), form.getParticipantID());
    }
    
    public CrossoverEligibilityCalculator( List<PulmonaryEvaluation> evals, Date screenDate, String participantID) throws PersistenceException {
        this.evals = evals;
        this.screenDate = screenDate;
        PersistenceManager pm = ServiceRegistry.getPersistenceManager();
        p = pm.getParticipant( participantID);
        index = screen();
    }
    
    /*
     * Use this constructor in tests.  Set the participant without a call to DB
     * and then call 'screen()'.
    */
    public CrossoverEligibilityCalculator( List<PulmonaryEvaluation> evals, Date screenDate) {
        this.evals = evals;
        this.screenDate = screenDate;
    }
    
    /*
     * handy for testing.
    */
    public void setParticipant( Participant p) {
        this.p = p;
    }
    
    public String getOutcomeMessage() throws PersistenceException {
        String msg;
        switch (index) {
            case 0:
                Date d = getEndOfHoldDate();
                SimpleDateFormat ft = new SimpleDateFormat ("MM-dd-yyyy");
                msg = String.format(msgs[0], ft.format(d));
                break;
            case 1:
                msg = msgs[1];
                break;
            case 2:
                msg = msgs[2];
                break;
            case 3:
                ft = new SimpleDateFormat ("MM-dd-yyyy");
                msg = String.format(msgs[3], ft.format( getStartCollectionOf4FEV1sDate()));
                break;
            case 4:
                msg = msgs[4];
                break;
            case 5:
                msg = msgs[5];
                break;
            case 6:
                msg = msgs[6];
                break;
            case 7:
                d = getDateOfLastEvaluation();
                ft = new SimpleDateFormat ("MM-dd-yyyy");
                msg = String.format(msgs[7], ft.format(d));
                break;
            default:
                msg = "Unknown outcome from CrossoverEligibilityCalculator: index = " + index;
                break;
        }
        return msg;
    }
    
    public boolean getDisplayCalculatorResults() {
        return displayCalculatorResults;
    }
    
    public String getOutcomeRule() {
        return rules[index];
    }
    
    public String[] getRules() {
        return rules;
    }
    
    public float getSlope() {
//        System.out.println("The slope is: " + eligCalc.getSlope() + "\n");
        return (float) eligCalc.getSlope();
    }
    
    public float getPValue() {
        return (float) eligCalc.getPValue();
    }
    
    public float getMinFev() {
        return eligCalc.getMinFev();
    }
    
    public Date getTimerStartDate() {
        if( p.getHoldStartDate() == null) {
            return p.getEnrolledDate();
        }
        else {
            return p.getHoldStartDate();
        }
    }

    public static Date getTimerStartDate( Participant p) {
        if( p.getHoldStartDate() == null) {
            return p.getEnrolledDate();
        }
        else {
            return p.getHoldStartDate();
        }
    }

    public Date getStartCollectionOf4FEV1sDate() {
        if( p.getHoldStartDate() == null) {
            return p.getEnrolledDate();
        }
        else {
            return getEndOfHoldDate();
        }
    }

    public Date getEndOfHoldDate() {
        Date d = getTimerStartDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add( Calendar.DAY_OF_MONTH, 60);
        return cal.getTime();
    }
    
    public static Date getEndOfHoldDate( Participant p) {
        Date d = getTimerStartDate(p);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add( Calendar.DAY_OF_MONTH, 60);
        return cal.getTime();
    }
    
    public Date getDateOfLastEvaluation() {
        Date d = null;
        if( evals.size() > 0) {
            d = evals.get(0).getDate();
            for( PulmonaryEvaluation e: evals) {
                if( e.getDate().after(d)) {
                    d = e.getDate();
                }
            }
        }
        return d;
    }
    
    public boolean isCrossoverEligible() {
        boolean b = false;
        if( index == 2 || index == 4) b = true;
        return b;
    }
    
    /**
     * careful!  isNotCrossoverEligilble does not equal ( ! isCrossoverEligble)
     * due to existence of override eligible state.
     * 
     * @return 
     */
    public boolean isNotCrossoverEligible() {
        boolean b = false;
        if( index == 0 || index == 1 || index == 3 || index == 7) b = true;
        return b;
    }
    
    public boolean isOverrideEligible() {
        boolean b = false;
        if( index == 5 ) b = true;
        return b;
    }
    
    public boolean isKeepParticipantInObservationalArmEligible() {
        boolean b = false;
        if( index == 6 ) b = true;
        return b;
    }
    

    public int screen() {
        eligCalc = new StudyArmEligibilityCalculator( evals, screenDate);
        int index;
                        
        if( p.isHoldStatus()) {
            index = 0;
            return index;
        }

        if( eligCalc.isDataStale()) {
            index = 7;
            return index;
        }
                
        if( eligCalc.isDataTooFew()) {
            index = 1;
            return index;
        }
        
        Date timerStartDate = getTimerStartDate();
        
        // Determine the number of FEV1s within the time period.
        if( getNumberOfMeasurementsInInterval( evals, timerStartDate, screenDate) < 4) {
            displayCalculatorResults = true;
            
            if( eligCalc.isSlopeOKandStatsSig()) {
                index = 2;
            }
            else {
                index = 3;
            }
        
            return index;
        }
        else {  // 4 or more FEVs in last 4 weeks.
            eligCalc = new StudyArmEligibilityCalculator( evals, screenDate);
            displayCalculatorResults = true;

            if( eligCalc.isECPTreatmentArmEligible()) {
                index = 4;
            }
            else if( eligCalc.isSlopeOKandStatsNotSig()){
                index = 5;
            } 
            else {  // Slope is not OK
                index = 6;
            }
        
            return index;
        }
    }
    
//    private int getNumberOfMeasurementsInInterval( List<PulmonaryEvaluation> evals, Date startDate, int periodInDays) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(startDate);
//        cal.add(Calendar.DAY_OF_MONTH, periodInDays);
//        Date stopDate = cal.getTime();
//        if( periodInDays >=0) {
//            return getNumberOfMeasurementsInInterval( evals, startDate, stopDate);
//        }
//        else {
//            return getNumberOfMeasurementsInInterval( evals, stopDate, startDate);
//        }
//        
//    }
    
    private int getNumberOfMeasurementsInInterval( List<PulmonaryEvaluation> evals, Date startDate, Date stopDate) {
        int count = 0;
        
        for( PulmonaryEvaluation eval: evals) {
            if( DateUtil.isDateInIntervalInclusive(eval.getDate(), startDate, stopDate)) {
                count++;
            }
        }
        return count;
    }
        
    public String getNextCrossoverDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_MONTH, 60);
        Date date = cal.getTime();
        SimpleDateFormat ft = new SimpleDateFormat ("MM-dd-yyyy");
        return ft.format(date);
    }
    
    // ToDo: this was kludged up in a hurry.  Refactor this when this calculator is integrated with original eligibility calculator.
    public static boolean isHoldExpired( Participant p, Date d) {
        Date hd = getEndOfHoldDate(p);
        return d.after(hd);
    }
    
}
