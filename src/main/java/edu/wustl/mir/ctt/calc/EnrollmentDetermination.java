package edu.wustl.mir.ctt.calc;

import edu.wustl.mir.ctt.form.EnrollmentForm;
import java.io.Serializable;

/**
 *
 * @author drm
 */
public class EnrollmentDetermination implements Serializable {
    
    private final String[] msgs = {
        "Patient is enrolled into the study. Enter FEV1s next to confirm eligibility",
        "Patient not eligible for enrollment into the study"
    };
    
    private final int index;
    
    public EnrollmentDetermination( EnrollmentForm form) {
        index = screen(form);
    }
    
    public String getOutcomeMessage() {
        return msgs[index];
    }
    
    
    public boolean isEnrollmentEligible() {
        boolean b = false;
        if( index == 0) b = true;
        return b;
    }
    
    public boolean isNotEnrollmentEligible() {
        boolean b = false;
        if( index == 1) b = true;
        return b;
    }
    
    
    private int screen(EnrollmentForm form) {
        int i = 0;  // If i = 0 at the end, inclusion and exclusion criteria all passed.
                    // Otherwise, if i = 1 at the end of the ifs, one or more criteria failed.
        
        if( form.getAge() != null ) {
            if("false".equals(form.getAge())){
                i = 1;
            }
        }
        if( form.getMedicare() != null ) {
            if("false".equals(form.getMedicare())){
                i = 1;
            }
        }
        if( form.getLungTransplant() != null ) {
            if("false".equals(form.getLungTransplant())){
                i = 1;
            }
        }
        if( form.getProgressiveBOS() != null ) {
            if("false".equals(form.getProgressiveBOS())){
                i = 1;
            }
        }
        if( form.getFiveFEV1sPostTrans() != null ) {
            if("false".equals(form.getFiveFEV1sPostTrans())){
                i = 1;
            }
        }
        if( form.getAnotherTrial() != null ) {
            if("true".equals(form.getAnotherTrial())){
                i = 1;
            }
        }
        if( form.getInterfereCondition() != null ) {
            if("true".equals(form.getInterfereCondition())){
                i = 1;
            }
        }
        if( form.getKnownAllergy() != null ) {
            if("true".equals(form.getKnownAllergy())){
                i = 1;
            }
        }
        if( form.getAcuteCondition() != null ) {
            if("true".equals(form.getAcuteCondition())){
                i = 1;
            }
        }
        if( form.getOtherCondition() != null ) {
            if("true".equals(form.getOtherCondition())){
                i = 1;
            }
        }
        if( form.getAphakia() != null ) {
            if("true".equals(form.getAphakia())){
                i = 1;
            }
        }
        if( form.getPregnancy() != null ) {
            if("true".equals(form.getPregnancy())){
                i = 1;
            }
        }
        if( form.getNoInformedConsent() != null ) {
            if("true".equals(form.getNoInformedConsent())){
                i = 1;
            }
        }
        if( form.getLeukopenia() != null ) {
            if("true".equals(form.getLeukopenia())){
                i = 1;
            }
        }
        return i;
    }
}