package edu.wustl.mir.ctt.directory;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;

/**
 *
 * @author drm
 */
public class LDAPDirectoryManager implements DirectoryManager {
    private Hashtable<String, String> env;
    private LdapContext ctx = null;
    
    private static final Logger logger = Logger.getLogger(LDAPDirectoryManager.class);

    public LDAPDirectoryManager(String ldapURL, String securityPrincipal, String securityCredentials) throws DirectoryManagerException {
        try {
            env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, ldapURL);
            if( securityPrincipal == null || "".equals(securityPrincipal)) {
                env.put(Context.SECURITY_AUTHENTICATION, "none");
            }
            else {
                env.put(Context.SECURITY_AUTHENTICATION, "simple");
                env.put(Context.SECURITY_PRINCIPAL, securityPrincipal);
                env.put(Context.SECURITY_CREDENTIALS, securityCredentials);
            }

            ctx = getContext();
        }
        catch( NamingException ex) {
            throw new DirectoryManagerException("Error instantiating LDAPDirectoryManager", ex);
        }
    }
    
    private LdapContext getContext() throws NamingException {
        if (ctx == null) {
            ctx = new InitialLdapContext(env, null);
            ctx.setRequestControls(null);
        }
        return ctx;
    }
    
    /**
     * Return the name of the current user's site.
     * 
     * @return
     * @throws DirectoryManagerException 
     */
    @Override
    public String getSiteName() throws DirectoryManagerException {
        String siteName = null;
        String userName = (String) SecurityUtils.getSubject().getPrincipal();
        
        if( userName != null) {
            List<String> siteNames = getSiteName(userName);
            if( siteNames.size() > 0) {
                siteName = siteNames.get(0);
            }
            if( siteNames.size() > 1) {
                logger.warn("user " + userName + " is associated with multiple sites: " + siteNames);
            }
            if( siteNames.size() == 0) {
                logger.warn("user " + userName + " is not associated with any site.");
            }
        }
        
        return siteName;
    }

    private List<String> getSiteNameOld( String userName) throws DirectoryManagerException {
        NamingEnumeration<?> namingEnum = null;
        try {
            List<String> siteNames = new ArrayList<String>();

            ctx = getContext();
            // Ugh. opendj 2.7 groups use objectclass groupOfNames with attribute of member.
            // opendj 2.6 groups use objectclass groupOfUniqueNames with attribute uniqueMember.
            // standardize on opendj 2.7
//            String filter = "(uniqueMember=cn=" + userName + ",ou=users,dc=ecpregistry,dc=wustl,dc=edu)";
//            String filter = "(member=cn=" + userName + ",ou=users,dc=ecpregistry,dc=wustl,dc=edu)";
            String filter = "(member=sAMAccountName=" + userName + ",ou=Users,ou=ECP Registry,dc=nrg,dc=mir)";

//            namingEnum = ctx.search("ou=sites,dc=ecpregistry,dc=wustl,dc=edu", filter, getSimpleSearchControls());
            namingEnum = ctx.search("ou=Sites,ou=ECP Registry,dc=nrg,dc=mir", filter, getSimpleSearchControls());

            while (namingEnum.hasMore()) {
                SearchResult result = (SearchResult) namingEnum.next();
                Attributes attrs = result.getAttributes();
                Attribute att = attrs.get("cn");
                siteNames.add( (att == null) ? "null" : (String) att.get());
            }
            return siteNames;
        } catch (Exception e) {
            String msg = "Error reading site names from LDAP directory.";
            throw new DirectoryManagerException( msg, e);
        }
        finally {
            if( namingEnum != null) try {namingEnum.close();} catch(NamingException ignore) {}
        }
    }

    private List<String> getSiteName( String userName) throws DirectoryManagerException {
        try {
            User user = getAccount( userName);
            return user.getSiteNames();
        } catch (Exception e) {
            String msg = "Error reading site names from LDAP directory.";
            throw new DirectoryManagerException( msg, e);
        }
    }

    @Override
    public List<User> getAccounts() throws DirectoryManagerException {
        NamingEnumeration<?> namingEnum = null;
        try {
            List<User> users = new ArrayList<User>();

            ctx = getContext();

            namingEnum = ctx.search("ou=users,ou=ECP Registry,dc=nrg,dc=mir", "(objectclass=*)", getSimpleSearchControls());

            while (namingEnum.hasMore()) {
                User user = new User();
                SearchResult result = (SearchResult) namingEnum.next();
                Attributes attrs = result.getAttributes();
                Attribute att = attrs.get("givenName");
                user.setFirstName( (att == null) ? "null" : (String) att.get());
                att = attrs.get("sn");
                user.setLastName( (att == null) ? "null" : (String) att.get());
                att = attrs.get("cn");
                user.setUserName( (att == null) ? "null" : (String) att.get());
                att = attrs.get("o");
                user.setOrganization( (att == null) ? "null" : (String) att.get());
                att = attrs.get("mail");
                user.setEmail( (att == null) ? "null" : (String) att.get());
                user.setSiteNames( getMemberOfSites( attrs));
                users.add( user);
            }
            return users;
        } catch (Exception e) {
            String msg = "Error reading accounts from LDAP directory.";
            throw new DirectoryManagerException( msg, e);
        }
        finally {
            if( namingEnum != null) try {namingEnum.close();} catch(NamingException ignore) {}
        }
    }

    public User getAccount( String userName) throws DirectoryManagerException {
        NamingEnumeration<?> namingEnum = null;
        try {

            ctx = getContext();

            namingEnum = ctx.search("ou=users,ou=ECP Registry,dc=nrg,dc=mir", "(sAMAccountName=" + userName+ ")", getSimpleSearchControls());

            User user = new User();
            while (namingEnum.hasMore()) {
                SearchResult result = (SearchResult) namingEnum.next();
                Attributes attrs = result.getAttributes();
                Attribute att = attrs.get("givenName");
                user.setFirstName( (att == null) ? "null" : (String) att.get());
                att = attrs.get("sn");
                user.setLastName( (att == null) ? "null" : (String) att.get());
                att = attrs.get("cn");
                user.setUserName( (att == null) ? "null" : (String) att.get());
                att = attrs.get("o");
                user.setOrganization( (att == null) ? "null" : (String) att.get());
                att = attrs.get("mail");
                user.setEmail( (att == null) ? "null" : (String) att.get());
                user.setSiteNames( getMemberOfSites( attrs));
            }
            return user;
        } catch (Exception e) {
            String msg = "Error reading accounts from LDAP directory.";
            throw new DirectoryManagerException( msg, e);
        }
        finally {
            if( namingEnum != null) try {namingEnum.close();} catch(NamingException ignore) {}
        }
    }

    /**
     * Return the name of the site to which this user is a member.
     *
     * The user's attributes can contain multiple 'memberof' entries as the user is both a member of groups and sites.
     * Filter the 'memberof' list for entries of type 'OU=site'.
     *
     * @param userAttributes
     * @return List of site names to which this user is a member.
     * @throws NamingException
     */
    private List<String> getMemberOfSites( Attributes userAttributes) throws NamingException {
        Attribute attribute = userAttributes.get("memberof");
        List<String> siteNames = new ArrayList<>();
        if( attribute != null) {
            NamingEnumeration<?> all = attribute.getAll();
            while (all.hasMore()) {
                String value = (String) all.next();
                if( value.contains("OU=Sites,")) {
                    String[] tokens = value.split(",");
                    if( tokens[0] != null && tokens[0].startsWith("CN=")) {
                        siteNames.add( tokens[0].substring(3));
                        break;
                    }
                }
            }
        }
        return siteNames;
    }

    @Override
    public void printAccounts(PrintStream ps) throws DirectoryManagerException {
        List<User> users = getAccounts();
        for( User user: users) {
            String s = user.getFirstName();
            ps.println("First Name: " + ((s == null) ? "null" : s));
            s = user.getLastName();
            ps.println("Last Name: " + ((s == null) ? "null" : s));
            s = user.getUserName();
            ps.println("User Name: " + ((s == null) ? "null" : s));
            s = user.getOrganization();
            ps.println("Organization: " + ((s == null) ? "null" : s));
            s = user.getEmail();
            ps.println("Email: " + ((s == null) ? "null" : s));
            ps.println();
        }
    }

    private static SearchControls getSimpleSearchControls() {
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setTimeLimit(30000);
        //String[] attrIDs = {"objectGUID"};
        //searchControls.setReturningAttributes(attrIDs);
        return searchControls;
    }

    /**
     * Test
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        try {
//            LDAPDirectoryManager ldap = new LDAPDirectoryManager("ldap://10.28.163.89:1389", "cn=Directory Manager", "DM'spw4ERL.");
//            LDAPDirectoryManager ldap = new LDAPDirectoryManager("ldap://localhost:1389", "cn=test_sc", "scecp");
//            LDAPDirectoryManager ldap = new LDAPDirectoryManager("ldap://localhost:1389", "cn=Directory Manager", "ldapDMbrussels");
//            LDAPDirectoryManager ldap = new LDAPDirectoryManager("ldap://localhost:1389", "cn=", "");
            LDAPDirectoryManager ldap = new LDAPDirectoryManager("ldaps://10.28.16.21", "ecp_ldap@nrg.mir", "Ehae1ahnah6iezahMah6rae9C");

//            System.out.println( ldap.getAccounts());
            System.out.println(ldap.getAccount("drm_sc"));

//            System.out.println( ldap.getSiteName("foo"));
//            ldap.printAccounts(System.out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
